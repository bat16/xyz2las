@ECHO OFF
setlocal enabledelayedexpansion
ECHO XYZ TO LAS
ECHO Author: Szczepkowski Marek
ECHO Date: 16-04-2020
ECHO Version: 1.0
ECHO QGIS 3.8


SET xyz2las="e:\MSZ\__SCRIPTS, COMMANDS\DTM_NDSM\DTN_DSM_nDSM\release_1.0.0\LAStools\\bin\txt2las.exe"


REM Path to working dir
SET WORK=%cd%



REM COUNTER FILES
dir /b *.xyz 2> nul | find "" /v /c > tmp && set /p countLAS=<tmp && del tmp
set /A Counter=1

FOR /F %%i IN ('dir /b "%WORK%\*.xyz"') DO (

	ECHO Processing  %%i....    !Counter! / %countLAS% FILES
	%xyz2las% -parse xyz -i "%WORK%\%%i" -o "%WORK%\%%~ni.las" -set_version 1.2
	
	set /A Counter+=1
)
ECHO.
ECHO Done
PAUSE