@ECHO OFF
setlocal enabledelayedexpansion
ECHO XYZ TO LAS
ECHO Author: Szczepkowski Marek
ECHO Date: 16-04-2020
ECHO Version: 1.0
ECHO QGIS 3.8


SET xyz2las="e:\MSZ\__SCRIPTS, COMMANDS\DTM_NDSM\DTN_DSM_nDSM\release_1.0.0\LAStools\\bin\txt2las.exe"


REM Path to working dir
SET WORK=%cd%


dir /b *.xyz > list.txt

%xyz2las% -lof list.txt -parse xyz -olas -cores 8 -i "%WORK%\%%i" -o "%WORK%\%%~ni.las" -set_version 1.2


ECHO.
ECHO Done
PAUSE